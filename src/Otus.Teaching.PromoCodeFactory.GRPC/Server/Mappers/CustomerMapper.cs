﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System;
using Otus.Teaching.PromoCodeFactory.GRPC;
using System.Linq;
using static Google.Rpc.Context.AttributeContext.Types;

namespace Server.Mappers
{
    public static class CustomerMapper
    {
        public static CustomerResponse MapFromModel(Customer customer)
        {
            var preferencesList = new List<PreferenceResponse>();

            if (customer.Preferences != null)
            {
                preferencesList.AddRange(customer.Preferences.Select(p => new PreferenceResponse
                {
                    Id = p.PreferenceId.ToString(),
                    Name = p.Preference.Name
                }));
            }

            CustomerResponse response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = { preferencesList } // Используем инициализатор свойства, чтобы задать список Preferences
            };

            return response;
        }

        public static Customer MapToModel(CreateOrEditCustomerRequest request, IEnumerable<Preference> preferences, bool existingCustomer, Customer customer = null)
        {
            Guid guid;
            if (!existingCustomer)
            {
                if (Guid.TryParse(request.Id, out var g))
                {
                    guid = g;
                }
                else
                    guid = Guid.NewGuid();
            }
            else
                guid = Guid.Parse(request.Id);

            if (customer == null)
            {
                customer = new Customer
                {

                    Id = guid,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                };
            }
            else
            {
                customer.FirstName = request.FirstName;
                customer.LastName = request.LastName;
                customer.Email = request.Email;
            }

            if (customer.Preferences != null)
                customer.Preferences.Clear();
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                Customer = customer,
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }
    }
}
