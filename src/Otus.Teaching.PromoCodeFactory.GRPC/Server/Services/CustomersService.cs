﻿using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GRPC;
using Server.Mappers; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        /// <summary>
        /// Конструктор сервиса
        /// </summary>
        /// <param name="customerRepository"></param>
        /// <param name="preferenceRepository"></param>
        public CustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        ///
        /// ```json
        /// {
        ///   "id": "a6c8c6b1-4349-45b0-ab31-244740aaf0f7",
        ///   "FirstName": "Петр",
        ///   "LastName": "Петров",
        ///   "Email": "dd@dd.dd",
        ///   "preferenceIds": [
        ///     {
        ///       "Id": "c4bda62e-fc74-4256-a956-4760b3858cbd"
        ///     },
        ///     {
        ///       "Id": "ef7f299f-92d7-459f-896e-078ed53ef99c"
        ///     }
        ///   ]
        /// }
        /// ```
        ///
        /// </remarks>
        /// <param name="request">Запрос</param>
        /// <param name="context">Контекст серверного вызова</param>
        /// <returns>Созданный клиент</returns>
        public override async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            List<Guid> guidList = request.PreferenceIds
                .Select(preferenceId => Guid.TryParse(preferenceId.Id, out Guid guid) ? guid : Guid.Empty)
                .Where(guid => guid != Guid.Empty)
                .ToList();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(guidList);

            Customer customer = CustomerMapper.MapToModel(request, preferences, false);

            await _customerRepository.AddAsync(customer);

            return CustomerMapper.MapFromModel(customer);
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        /// ```json
        ///a6c8c6b1-4349-45b0-ab31-244740aaf0f0
        /// ```
        /// </remarks>
        /// <param name="request">Запрос</param>
        /// <param name="context">Контекст серверного вызова</param>
        /// <returns>Клиент</returns>
        public override async Task<DeleteCustomerResponse> DeleteCustomer(DeleteCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdWithIncludesAsync<Customer>(Guid.Parse(request.Id), c => c.Preferences);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            await _customerRepository.DeleteAsync(customer);

            return new DeleteCustomerResponse();
        }

        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        /// ```json
        /// {
        ///  "id": "a6c8c6b1-4349-45b0-ab31-244740aaf0f0",
        ///  "FirstName": "Иван-редактирован",
        ///  "LastName": "Петров-редактирован",
        ///  "Email": "ivan_sergeev@mail.ru",
        ///   "preferenceIds": [
        ///      {
        ///         "Id": "c4bda62e-fc74-4256-a956-4760b3858cbd"
        ///      },
        ///      {
        ///         "Id": "ef7f299f-92d7-459f-896e-078ed53ef99c"
        ///      }
        ///   ]
        /// }
        /// ```
        ///
        /// </remarks>
        /// <param name="request">Запрос</param>
        /// <param name="context">Контекст серверного вызова</param>
        /// <returns>Созданный клиент</returns>
        public override async Task<CustomerResponse> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            List<Guid> guidList = request.PreferenceIds
                .Select(preferenceId => Guid.TryParse(preferenceId.Id, out Guid guid) ? guid : Guid.Empty)
                .Where(guid => guid != Guid.Empty)
                .ToList();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(guidList);

            //var existingCustomer = await _customerRepository.GetByIdAsync(Guid.Parse(context.GetHttpContext().Request.RouteValues["id"].ToString()));
            var customer = await _customerRepository.GetByIdWithIncludesAsync<Customer>(Guid.Parse(request.Id), c => c.Preferences);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            CustomerMapper.MapToModel(request, preferences, true, customer);

            await _customerRepository.UpdateAsync(customer);

            return CustomerMapper.MapFromModel(customer);
        }

        /// <summary>
        /// Получить клиента по ID
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        /// ```json
        ///a6c8c6b1-4349-45b0-ab31-244740aaf0f0
        /// ```
        /// </remarks>
        /// <param name="request">Запрос</param>
        /// <param name="context">Контекст серверного вызова</param>
        /// <returns>Клиент</returns>
        public override async Task<CustomerResponse> GetCustomer(CustomerRequest request, ServerCallContext context)
        {
            //var customerId = Guid.Parse(context.GetHttpContext().Request.RouteValues["id"].ToString());
            var customerId = Guid.Parse(request.Id);
            
            // только таким образом грузятся вложенные сущности (не дальше 1 уровня, поэтому остальное руками отдельным запросом ниже)
            var customer = await _customerRepository.GetByIdWithIncludesAsync<Customer>(customerId, c => c.Preferences);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            // Отдельный запрос для загрузки связанных сущностей Preference
            var preferences = customer.Preferences.ToList();
            foreach (var preference in preferences)
            {
                preference.Preference = await _preferenceRepository.GetByIdAsync(preference.PreferenceId);
            }

            return CustomerMapper.MapFromModel(customer);
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <param name="request">Запрос</param>
        /// <param name="context">Контекст серверного вызова</param>
        /// <returns>Все клиенты</returns>
        public override async Task<CustomersResponse> GetCustomers(CustomersRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersResponse = new CustomersResponse
            {
                Customers = { customers.Select(x => new CustomerShortResponse
                {
                    Id = x.Id.ToString(),
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }) }
            };

            return customersResponse;
        }
    }
}
