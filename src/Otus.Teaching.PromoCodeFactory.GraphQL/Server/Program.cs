using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Server.Customers;
using Server.Types;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<CustomersQueries>();

builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();
builder.Services.AddDbContext<DataContext>(x =>
{
    x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
});

builder.Services
    .AddGraphQLServer()
    .AddQueryType<CustomersQueries>()
    .AddMutationType<CustomersMutations>()
    .AddType<CustomerType>()
    .AddType<PreferenceType>();

var app = builder.Build();

// ������������� ��
var serviceProvider = app.Services;
using (var scope = serviceProvider.CreateScope())
{
    var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
    dbInitializer.InitializeDb();
}

app.MapGraphQL();

app.Run();

/*
 
query {
  customers {
    id
    firstName
    lastName
    email
  }
}

query {
  customer(id: "a6c8c6b1-4349-45b0-ab31-244740aaf0f0") {
    id
    firstName
    lastName
    email
    preferences {
      preference {
        id
        name
      }
    }
  }
}

mutation {
  createCustomer(request: {
    firstName: "���",
    lastName: "�������",
    email: "example@example.com",
    preferenceIds: ["ef7f299f-92d7-459f-896e-078ed53ef99c", "c4bda62e-fc74-4256-a956-4760b3858cbd"]
  }) {
    id
    firstName
    lastName
    email
  }
}

mutation {
  editCustomer(id: "a6c8c6b1-4349-45b0-ab31-244740aaf0f0", request: {
    firstName: "���� - ������������",
    lastName: "������ - ������������",
    email: "ivan_sergeev@mail.ru",
    preferenceIds: ["ef7f299f-92d7-459f-896e-078ed53ef99c"]
  }) {
    id
    firstName
    lastName
    email
    preferences {
      preferenceId
    }
  }
}

mutation {
  deleteCustomer(id: "a6c8c6b1-4349-45b0-ab31-244740aaf0f0")
}
 
 */