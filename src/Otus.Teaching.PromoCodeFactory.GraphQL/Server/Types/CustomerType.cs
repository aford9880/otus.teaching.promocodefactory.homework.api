﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Server.Types
{
    public class CustomerType : ObjectType<Customer>
    {
        protected override void Configure(IObjectTypeDescriptor<Customer> descriptor)
        {
            descriptor.Field(c => c.Id).Type<NonNullType<UuidType>>();
            descriptor.Field(c => c.FirstName).Type<NonNullType<StringType>>();
            descriptor.Field(c => c.LastName).Type<NonNullType<StringType>>();
            descriptor.Field(c => c.Email).Type<NonNullType<StringType>>();

            descriptor.Field("preferences")
                .Type<ListType<CustomerPreferenceType>>()
                .Resolve(context =>
                {
                    var customer = context.Parent<Customer>();
                    return customer.Preferences;
                });
        }
    }
}
