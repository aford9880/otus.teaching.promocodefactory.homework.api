﻿using Server.Customers;
using Server.Models;

namespace Server.Types
{
    public class MutationType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Name("Mutation");

            descriptor.Field("createCustomer")
                .Argument("request", arg => arg.Type<NonNullType<CreateOrEditCustomerInputType>>())
                .Type<CustomerType>()
                .Resolve(async context =>
                {
                    var request = context.ArgumentValue<CreateOrEditCustomerRequest>("request");
                    return await context.Service<CustomersMutations>().CreateCustomerAsync(request);
                });

            descriptor.Field("editCustomer")
                .Argument("id", arg => arg.Type<NonNullType<UuidType>>())
                .Argument("request", arg => arg.Type<NonNullType<CreateOrEditCustomerInputType>>())
                .Type<CustomerType>()
                .Resolve(async context =>
                {
                    var id = context.ArgumentValue<Guid>("id");
                    var request = context.ArgumentValue<CreateOrEditCustomerRequest>("request");
                    return await context.Service<CustomersMutations>().EditCustomerAsync(id, request);
                });

            descriptor.Field("deleteCustomer")
                .Argument("id", arg => arg.Type<NonNullType<UuidType>>())
                .Type<BooleanType>()
                .Resolve(async context =>
                {
                    var id = context.ArgumentValue<Guid>("id");
                    return await context.Service<CustomersMutations>().DeleteCustomerAsync(id);
                });
        }
    }
}
