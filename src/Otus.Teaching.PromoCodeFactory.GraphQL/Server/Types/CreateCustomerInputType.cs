﻿using Server.Models;

namespace Server.Types
{
    public class CreateOrEditCustomerInputType : InputObjectType<CreateOrEditCustomerRequest>
    {
        protected override void Configure(IInputObjectTypeDescriptor<CreateOrEditCustomerRequest> descriptor)
        {
            descriptor.Field(c => c.FirstName).Type<NonNullType<StringType>>();
            descriptor.Field(c => c.LastName).Type<NonNullType<StringType>>();
            descriptor.Field(c => c.Email).Type<NonNullType<StringType>>();
            descriptor.Field(c => c.PreferenceIds).Type<NonNullType<ListType<NonNullType<UuidType>>>>();
        }
    }
}
