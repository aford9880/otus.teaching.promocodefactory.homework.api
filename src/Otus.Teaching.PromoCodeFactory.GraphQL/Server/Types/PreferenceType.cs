﻿using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Server.Types
{
    public class PreferenceType : ObjectType<Preference>
    {
        protected override void Configure(IObjectTypeDescriptor<Preference> descriptor)
        {
            descriptor.Field(p => p.Id).Type<NonNullType<UuidType>>();
            descriptor.Field(p => p.Name).Type<NonNullType<StringType>>();

        }
    }

    public class CustomerPreferenceType : ObjectType<CustomerPreference>
    {
        protected override void Configure(IObjectTypeDescriptor<CustomerPreference> descriptor)
        {
            descriptor.Field(p => p.PreferenceId).Type<NonNullType<UuidType>>();

            descriptor.Field("preference")
                .Type<PreferenceType>()
                .Resolve(context =>
                {
                    var customerPreference = context.Parent<CustomerPreference>();
                    var preference = customerPreference.Preference;
                    return preference;
                });
        }
    }
}
