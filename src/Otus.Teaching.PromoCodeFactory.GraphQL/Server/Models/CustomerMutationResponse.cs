﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Server.Models
{
    public class CustomerMutationResponse
    {
        public Customer Customer { get; set; }
        public List<string> Errors { get; set; }
    }
}
