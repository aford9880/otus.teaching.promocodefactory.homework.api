﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Server.Types;

namespace Server.Customers
{
    public class CustomersQueries
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersQueries(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            return customers;
        }

        public async Task<Customer> GetCustomerAsync(Guid id/*, [Service] IRepository<Customer> _customerRepository*/)
        {
            //var customer = await _customerRepository.GetByIdAsync(id);
            var customer = await _customerRepository.GetByIdWithIncludesAsync<Customer>(id, c => c.Preferences);

            // Отдельный запрос для загрузки связанных сущностей Preference
            var preferences = customer.Preferences.ToList();
            foreach (var preference in preferences)
            {
                preference.Preference = await _preferenceRepository.GetByIdAsync(preference.PreferenceId);
            }

            return customer;
        }
    }
}
