﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Server.Models;
using Server.Types;

namespace Server.Customers
{
    public class CustomersMutations
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersMutations(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<Customer> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            Customer customer = new Customer();
            customer.Id = Guid.NewGuid();
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.AddAsync(customer);

            return customer;
        }

        public async Task<Customer> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdWithIncludesAsync<Customer>(id, c => c.Preferences);
            if (customer == null)
                throw new Exception("Customer not found.");

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            if (customer.Preferences != null)
                customer.Preferences.Clear();

            if (request.PreferenceIds != null)
            {
                var preferences = request.PreferenceIds.ToList();
                foreach (var preferenceId in preferences)
                {
                    var preference = await _preferenceRepository.GetByIdAsync(preferenceId);
                    
                    if (preference == null)
                        throw new Exception($"Could not find {preferenceId}");

                    if (customer.Preferences == null)
                        customer.Preferences = new List<CustomerPreference>();

                    customer.Preferences.Add(new CustomerPreference {
                        Customer = customer,
                        CustomerId = customer.Id,
                        Preference = preference,
                        PreferenceId = preferenceId,
                    });
                }
            }
            await _customerRepository.UpdateAsync(customer);
            return customer;
        }

        public async Task<bool> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdWithIncludesAsync<Customer>(id, c => c.Preferences);
            if (customer == null)
                throw new Exception("Customer not found.");

            await _customerRepository.DeleteAsync(customer);
            return true;
        }
    }
}
